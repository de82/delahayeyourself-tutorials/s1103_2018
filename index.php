<!DOCTYPE html>
<html lang="fr" class="full">

<head>
    <title>S1103 - Exemples simple pour apprendre le PHP</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="assets/style.css" />
</head>

    <?php
    $dirs = array_filter(glob('*'), 'is_dir');
    $dirs = preg_grep('/^tp(.*)/', $dirs);
    $key = 9;
    ?>

<body class="full">
    <header>
        <h1>S1103</h1>
        <h2>Exemples simples pour apprendre le PHP</h2>
        <hr />
        <nav>
            <?php foreach ($dirs as $dir): ?>
                <?php $key++;?>
                <a href="<?php echo $dir; ?>/">TP n°
                    <?php echo $key; ?>
                </a>
            <?php endforeach;?>
        </nav>
    </header>
</body>

</html>