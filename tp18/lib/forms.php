<?php defined('ACCESSIBLE') or die('No direct script access.');

function getDataFromPost($arr_parameters) {
    $data = array();
    foreach($arr_parameters as $attr => $type){
        if($type == 'int'){
            $value = intval($_POST[$attr]);
        }else{
            $value = htmlspecialchars($_POST[$attr]);
        }
        $data[$attr] = $value;
    }
    return $data;
}

function checkEmpty($attr){
    if(empty($_POST[$attr])){
        return true;
    }
    return false;
}


function checkRequiredFields($arr_parameters_required){
    $required_field_set = true;
    foreach($arr_parameters_required as $attr){
        if(checkEmpty($attr)){
            $required_field_set = false;
        }
    }
    return $required_field_set;
}


function getValueFromPost($attr){
    if($_POST and isset($_POST[$attr])){
        return $_POST[$attr];
    }
    return "";
}


function treatmentFormDinosaur($database, $arr_parameters, $arr_parameters_required){
    if($_POST){
        //Parameters from form
        $data = getDataFromPost($arr_parameters);


        if (!checkRequiredFields($arr_parameters_required)){
            return 'Tous les champs doivent être renseigné !';
        }

        if(dinosaurExist($database, $data['name'])){
            updateDinosaur($database, $data['name'], $data['birthday'], $data['model_id']);
            return sprintf("Dinosaur %s mis à jour !", $data['name']);
        }else{
            createDinosaur($database, $data['name'], $data['birthday'], $data['model_id']);
            return sprintf("Dinosaur %s ajouté !", $data['name']);
        }

    }

}