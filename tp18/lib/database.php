<?php defined('ACCESSIBLE') or die('No direct script access.');

function getDatabaseConnexion(){
    try {
        $db = new PDO('sqlite:' . DB_NAME);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }catch (Exception $e){
        exit('Erreur : '. $e->getMessage());
    }
}


function prepare_and_execute_query($database, $sql, $parameters){
    $query = $database->prepare($sql);
    if($parameters){
        $query->execute($parameters);
    }else{
        $query->execute();
    }
    return $query;
}


function query($database, $sql, $parameters=null){
    $query = prepare_and_execute_query($database, $sql, $parameters);
    $results = $query->fetchAll(PDO::FETCH_ASSOC);
    $query->closeCursor();
    return $results;
}

function queryNoResult($database, $sql, $parameters=null) {
    prepare_and_execute_query($database, $sql, $parameters);
}

function queryOne($database, $sql, $parameters=null){
    $query = prepare_and_execute_query($database, $sql, $parameters);
    $results = $query->fetch(PDO::FETCH_ASSOC);
    $query->closeCursor();
    return $results;
}
