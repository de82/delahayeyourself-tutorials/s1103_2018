<?php defined('ACCESSIBLE') or die('No direct script access.');


function getAllDinosaursWithModelAndSpecie($database) {
    $sql = 'SELECT dinosaurs.id AS id, dinosaurs.name AS name, birthday,
            species.name AS specie, version
            FROM dinosaurs, species , models
            WHERE dinosaurs.model_id = models.id
            AND models.specie_id == species.id';
    return query($database, $sql);
}


function getAllDinosaurs($database) {
    $sql = 'SELECT id, name, birthday FROM dinosaurs';
    return query($database, $sql);
}


function countDinosaurs($database) {
    $sql = 'SELECT count(id) AS total FROM dinosaurs';
    return queryOne($database, $sql);
}


function dinosaurExist($database, $name) {
    $sql = 'SELECT count(id) AS total FROM dinosaurs WHERE name = :name';
    $total = queryOne($database, $sql, array('name' => $name));
    if($total['total'] > 0){
        return true;
    }else{
        return false;
    }
}

function createDinosaur($database, $name, $birthday, $model_id) {
    $sql = 'INSERT INTO dinosaurs (name, birthday, model_id)
            VALUES (:name, :birthday, :model_id)';
    $parameters = array(
        'name' => $name,
        'birthday' => $birthday,
        'model_id' => $model_id,
    );
    queryNoResult($database, $sql, $parameters);
}


function updateDinosaur($database, $name, $birthday, $model_id) {
    $sql = 'UPDATE dinosaurs
            SET name = :name,
            birthday = :birthday,
            model_id = :model_id
            WHERE name = :name';
    $parameters = array(
        'name' => $name,
        'birthday' => $birthday,
        'model_id' => $model_id,
    );
    queryNoResult($database, $sql, $parameters);
}