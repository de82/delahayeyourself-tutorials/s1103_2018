<?php defined('ACCESSIBLE') or die('No direct script access.');

function getAllModels($database){
    $sql = 'SELECT models.id AS id, version, species.name AS specie
            FROM species, models 
            WHERE models.specie_id = species.id';
    return query($database, $sql);
}
