<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°18 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php require_once 'lib/const.php'; ?>
    <?php include 'parts/_header.php';?>
    <?php require_once 'lib/database.php'; ?>
    <?php require_once 'lib/dinosaurs.php'; ?>
    <?php require_once 'lib/models.php'; ?>
    <?php require_once 'lib/forms.php'; ?>
    <main>
        <?php 
        $database = getDatabaseConnexion();
        $models = getAllModels($database);
        $arr_parameters = array(
            'name' => 'text',
            'birthday' => 'text',
            'model_id' => 'int',
        );
        $arr_parameters_required = array(
            'name',
            'birthday',
            'model_id',
        );
        ?>

        <blockquote>
            <?php echo treatmentFormDinosaur($database, $arr_parameters, $arr_parameters_required); ?>
        </blockquote>

        <h2>Ajouter un dinosaure !</h2>
        <hr />
        <form action="" method="POST">
            <label for="name">Nom</label>
            <input type="text" name="name" value="<?php echo getValueFromPost("name"); ?>" placeholder="Denver" />

            <label for="birthday">Date de naissance</label>
            <input type="date" name="birthday" value="<?php echo getValueFromPost("birthday"); ?>" placeholder="04/05/1977" />

            <label for="model_id">Modèle</label>
            <select name="model_id">
                <?php $selected = getValueFromPost("model_id"); ?>
                <?php foreach($models as $model): ?>
                <option <?php if($selected == $model['id']): ?>selected="selected"<?php endif; ?> value="<?php echo $model['id']; ?>">
                    <?php echo $model['specie']; ?> v. <?php echo $model['version']; ?>
                </option>
                <?php endforeach; ?>
            </select>
            <button type="submit">Ajouter le dinosaure</button>
        </form>
    </main>
</body>

</html>