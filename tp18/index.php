<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°18 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php require_once 'lib/const.php'; ?>
    <?php include 'parts/_header.php';?>
    <?php require_once 'lib/database.php'; ?>
    <?php require_once 'lib/dinosaurs.php'; ?>
    <main>

        <?php 

        $database = getDatabaseConnexion();

        ?>

        <section>
            <h2>Quelques requêtes sur la base de données d'InGen</h2>
            <article>
                <h3>Les dinosaures ..</h3>

                <?php
                $results = getAllDinosaurs($database); ?>
                <?php foreach($results as $result): ?>
                    <p>
                    <?php foreach($result as $key => $value): ?>
                        <em><?php echo $key; ?></em>:
                        <code><?php echo $value; ?></code>,
                    <?php endforeach; ?>
                    </p>
                <?php endforeach; ?>
            </article>

            <article>
                <h3>Combien de dinosaures y'a t'il dans la base de données ? </h3>

                <?php
                $result = countDinosaurs($database); ?>
                <p>
                <?php foreach($result as $key => $value): ?>
                    <em><?php echo $key; ?></em>:
                    <code><?php echo $value; ?></code>,
                <?php endforeach; ?>
                </p>
            </article>

            <article>
                <h3>Les dinosaures .. (avec jointures)</h3>

                <?php
                
                $results = getAllDinosaursWithModelAndSpecie($database); ?>
                    <?php foreach($results as $result): ?>
                        <p>
                        <?php foreach($result as $key => $value): ?>
                            <em><?php echo $key; ?></em>:
                            <code><?php echo $value; ?></code>,
                        <?php endforeach; ?>
                        </p>
                    <?php endforeach; ?>
            </article>
        </section>
    </main>
</body>

</html>