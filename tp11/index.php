<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°11 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php include '_header.php';?>
    <?php require_once 'data.php'; ?>

    <main>

        <h2>Parcours de $arr_persos1</h2>
        <!-- Attention mauvaise utilisation ici -->
        <?php for($index=0; $index<count($arr_persos1); $index++): ?>
            <h3>Index num <code><?php echo $index; ?></code></h3>
            <p>
                Contenu de $arr_persos1[$index]: 
                <code><?php echo $arr_persos1[$index]; ?></code>
            </p>
        <?php endfor; ?>


        <?php foreach($arr_persos1 as $index => $perso): ?>
            <h3>Index num <code><?php echo $index; ?></code></h3>
            <p>
                Contenu de $perso: 
                <code><?php echo $perso; ?></code>
            </p>
        <?php endforeach; ?>

        <hr />

        <h2>Parcours de $arr_persos2</h2>
        <?php foreach($arr_persos2 as $index => $perso): ?>
            <h3>Index <code><?php echo $index; ?></code></h3>
            <p>
                Contenu de $perso: 
                <code><?php echo $perso; ?></code>
            </p>
        <?php endforeach; ?>


        <hr />

        <h2>Parcours de $arr_persos3</h2>

        <?php foreach($arr_persos3 as $key => $perso): ?>

        <h3>Personnage n° <code><?php echo $key; ?></code></h3>

            <dl>
            <?php foreach($perso as $title => $content): ?>
                <dt><?php echo $title; ?></dt>

                <dd><?php echo $content; ?></dd>
            <?php endforeach; ?>
            </dl>

        <?php endforeach; ?>

        <hr />

        <h2>&laquo;Affichage&raquo; d'une commande</h2>

        <?php
        $tab = array();
        $tab['prix_unitaire'] = 12;
        $tab['quantite'] = 5;
        ?>

        <p>Vous avez commandé articles <code><?php echo $tab['quantite']; ?></code> au prix unitaire de <code><?php echo $tab['prix_unitaire']; ?></code> soit un total de <code><?php echo $tab['quantite'] * $tab['prix_unitaire']; ?></code> €.</p>


    </main>


</body>

</html>