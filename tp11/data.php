<?php
$arr_persos1 = array(
    'Luke',
    'Han',
    'Leia',
    'Ben',
);

$arr_persos2 = array();
$arr_persos2[0] = 'Ben';
$arr_persos2[1] = 'Luke';
$arr_persos2[2] = 'Han';
$arr_persos2['Organa'] = 'Leia';

$luke = array(
    'nom' => 'Skywalker',
    'prenom' => 'Luke',
    'job' => 'unknow',
);

$ben = array(
    'nom' => 'Kenobi',
    'prenom' => 'Ben',
    'job' => 'Old man euh.. old jedi master',
);

$han = array(
    'nom' => 'Solo',
    'prenom' => 'Han',
    'job' => 'smuggler',
);

$arr_persos3 = array(
    $luke,
    $ben,
    $han,
);