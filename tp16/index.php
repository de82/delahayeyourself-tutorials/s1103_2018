<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°16 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php require_once 'const.php'; ?>
    <?php include '_header.php';?>

    <main>
    <h1>Form GET</h1>
    <form action="traitement.php" method="GET">
        <div class="form-example">
            <label for="name">Enter your name: </label>
            <input type="text" name="name" id="name" placeholder="Kenobi" required>
        </div>
        <div class="form-example">
            <label for="firstname">Enter your firstname: </label>
            <input type="text" name="firstname" id="firstname" placeholder="Ben" required>
        </div>
        <div class="form-example">
            <label for="email">Enter your email: </label>
            <input type="email" name="email" id="email" placeholder="vieuxben@kenobi.ga" required>
        </div>
        <div class="form-example">
            <label for="age">Enter your age: </label>
            <input type="number" name="age" id="age" placeholder="78" required>
        </div>
        <div class="form-example">
            <label for="job">Enter your job: </label>
            <select id="job" name="job" required>
                <option>--Please choose an option--</option>
                <?php foreach(JOBS as $key => $value): ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-example">
            <label for="description">Enter your description: </label>
            <textarea name="description" id="description" placeholder="It's my life" required></textarea>
        </div>
        
        <div class="form-example">
            <input type="submit" value="Subscribe!">
        </div>
    </form>

    </main>


</body>

</html>