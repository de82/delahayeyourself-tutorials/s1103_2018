<?php
    define('ACCESSIBLE', '1');
    define('JOBS', array(
        'jedi' => 'Jedi',
        'farmer' => 'Farmer',
        'smuggler' => 'Smuggler',
        'other' => 'Other',
    ));

    define('QUOTIENT_FAMILIAL', array(
        5615 => 5.5,
        11199 => 14,
        24873 => 30,
        66680 => 40,
    ));
