<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°16 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php require_once 'const.php'; ?>
    <?php require_once 'functions.php'; ?>
    <?php include '_header.php';?>

    <main>
        <h1>Impots</h1>
        <hr />
        <form action="impots.php" method="GET">
            <div class="form-example">
                <label for="nb_enfants">Nombre d'enfants: </label>
                <input type="number" name="nb_enfants" id="nb_enfants" placeholder="Nombre d'enfants" required>
            </div>
            <div class="form-example">
                <label for="situation">Situation: </label>
                <select id="situation" name="situation" required>
                    <option value="single">Célibataire</option>
                    <option value="maried">Marié</option>
                </select>
            </div>

            <div class="form-example">
                <label for="revenu">Revenu: </label>
                <input type="number" name="revenu" id="revenu" placeholder="Revenu" required>
            </div>
            <div class="form-example">
                <input type="submit" value="Calcul!">
            </div>
        </form>

        <?php if(isset($_GET['nb_enfants']) and isset($_GET['situation']) and isset($_GET['revenu'])): ?>
            <h1>Simulation</h1>
            <hr />
            <?php show_params_from_form(array(
                'revenu' => 'int',
                'situation' => 'string',
                'nb_enfants' => 'int',
            )); 
            
            $nb_enfants = intval($_GET['nb_enfants']);
            $revenu = intval($_GET['revenu']);
            $situation = $_GET['situation'];
            
            $parts = calcul_nb_parts($nb_enfants, $situation); 
            $revenu_imposable = calcul_revenu_imposable($revenu);
            $quotient = calcul_quotient_familial($parts, $revenu_imposable);
            $tranche = calcul_tranche_imposition($quotient);
            ?>

            <h2>Calcul du nombre de part</h2>
            <dl>
                <?php if($situation == 'single'): ?>
                <dt>parts = nbEnfants/2+1</dt>
                <?php else: ?>
                <dt>parts = nbEnfants/2+2</dt>
                <?php endif; ?>
                
                <dd><code>parts = <?php echo $parts; ?></code></dd>
            </dl>

            <h2>Calcul du revenu imposable</h2>
            <dl>
                <dt>R = 0.72 * S</dt>
                <dd><code>R = <?php echo $revenu_imposable; ?></code></dd>
            </dl>

            <h2>Calcul du quotient familial</h2>
            <dl>
                <dt>Q = R / parts</dt>
                <dd><code>Q = <?php echo $quotient; ?></code></dd>
            </dl>

            <h2>Calcul de la tranche</h2>

            <table>
                <thead>
                    <tr>
                        <th>0 à 5614</th>
                        <th>5615 à 11198</th>
                        <th>11199 à 24872</th>
                        <th>24873 à 66679</th>
                        <th>66680 et plus</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>0%</td>
                        <td>5.5%</td>
                        <td>14%</td>
                        <td>30%</td>
                        <td>40%</td>
                    </tr>
                </tbody>
            </table>
            <dl>
                <dt>cf. tableau ci-dessus</dt>
                <dd><code>tranche en % = <?php echo $tranche; ?></code></dd>
            </dl>

            <h2>Simulation de l'impot sur le revenu</h2>
            <dl>
                <dt>tranche * revenu impossable</dt>
                <dd><code>Taxe sur le revenu = <?php echo $revenu_imposable * ($tranche/100); ?></code> €</dd>
            </dl>
        <?php
        endif;
        ?>
    </main>


</body>

</html>