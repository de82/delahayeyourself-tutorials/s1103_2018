<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°16 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php require_once 'const.php'; ?>
    <?php require_once 'functions.php'; ?>
    <?php include '_header.php';?>

    <main>
        <h2>Page de traitement</h2>
        <?php
            $inputs = array(
                'name' => 'string',
                'firstname' => 'string',
                'email' => 'string',
                'job' => 'string',
                'description' => 'string',
                'age' => 'int',
            );
        ?>
        
        <hr />
        <h3>Data from $_GET</h3>
        <hr />
        <?php show_params_from_form($inputs); ?>
        <hr />
        <h3>Data from $_POST</h3>
        <hr />
        <?php show_params_from_form($inputs, 'post'); ?>
        
    </main>


</body>

</html>