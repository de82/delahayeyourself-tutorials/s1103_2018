<?php defined('ACCESSIBLE') or die('No direct script access.');

function show_params_from_form($inputs, $method='GET'){

    if ($method == 'GET'){
        $parameters = $_GET;
    }else{
        $parameters = $_POST;
    }

    foreach($inputs as $name => $type):
        if(isset($parameters[$name])):
            $value = $parameters[$name];
            if($type == 'int'){
                $value = intval($value);
            }
        ?>
            <p>
            <code><?php echo $name; ?></code>: <em><?php echo $value; ?></em>
            </p>
    <?php 
        endif;
    endforeach; 
}


function calcul_nb_parts($nb_enfant, $situation){
    if($situation == 'single'){
        $nb_adultes = 1;
    }else{
        $nb_adultes = 2;
    }
    return $nb_enfant/2 + $nb_adultes;
}

function calcul_revenu_imposable($revenu){
    return 0.72 * $revenu;
}

function calcul_quotient_familial($parts, $revenu_imposable){
    return $revenu_imposable / $parts;
}

function calcul_tranche_imposition($quotient_familial){
    $tranche = 0;
    foreach(QUOTIENT_FAMILIAL as $quotient_max => $tranche_quotient){
        if($quotient_familial >= $quotient_max){
            $tranche = $tranche_quotient;
        }
    }
    return $tranche;
}