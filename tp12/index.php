<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°12 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php include '_header.php';?>
    <?php require_once 'functions.php'; ?>

    <main>

        <h2>Utilisation de fonctions persos!</h2>

        <p>Appel de sayHello: <code><?php echo sayHello('Ben'); ?></code></p>
        <p>Appel de sayHello: <code><?php echo sayHello('Luke'); ?></code></p>
        <p>Appel de sayHello: <code><?php echo sayHello('Han'); ?></code></p>
        <p>Appel de sayHello: <code><?php echo sayHello(); ?></code></p>
        <hr />


        <h2>Utilisation de fonctions sur les strings!</h2>

        <?php $string = 'Hell0W0rld'; ?>

        <p>Contenu de $string: <code><?php echo $string; ?></code></p>

        <p>Longueur de $string: <code><?php echo strlen($string); ?></code></p>

        <p>str_replace ? <code><?php echo str_replace('0', 'o', $string); ?></code></p>


        <p>str_shuffle ? <code><?php echo str_shuffle($string); ?></code></p>

        <p>strtoupper ? <code><?php echo strtoupper($string); ?></code></p>

        <p>strtolower ? <code><?php echo strtolower($string); ?></code></p>
        

    </main>


</body>

</html>