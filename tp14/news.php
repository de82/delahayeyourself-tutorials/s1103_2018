<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°14 - News.php!</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>

    <?php require_once 'const.php'; ?>
    <?php require_once 'functions.php'; ?>
    <?php include '_header.php';?>
    <main>
        <h2>News</h2>

        <p><code><?php echo sayHello('News'); ?></code></p>

    </main>


</body>

</html>