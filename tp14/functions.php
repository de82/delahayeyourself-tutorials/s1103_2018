<?php defined('ACCESSIBLE') or die('No direct script access.');

function sayHello($name=null){
    if($name == null){
        return 'Hello John Doe';
    }
    return "Hello  $name";
}