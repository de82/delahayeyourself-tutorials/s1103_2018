<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°10 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php include '_header.php';?>

    <main>
        <h1><?php echo ('Ceci est la page index'); ?></h1>

        <?php $ma_jolie_variable = "Ceci est ma jolie variable";?>
        <p>Contenu de $ma_jolie_variable: <code><?php echo $ma_jolie_variable; ?></code></p>

        <?php $name = "Garfield";?>

        <?php $ma_jolie_variable = 'Hello ' . $name;?>

        <p>Contenu de $ma_jolie_variable: <code><?php echo $ma_jolie_variable; ?></code></p>

        <p>Une simple boucle ? </p>
        <ul>
            <?php for ($i = 0; $i <= 10; $i++): ?>
            <li>
                <?php echo $i; ?>
            </li>
            <?php endfor;?>
        </ul>

        <?php $valeur = 42;?>

        <?php if ($valeur < 0): ?>
            <p><code>$valeur</code> est inférieur à 0</p>
        <?php else: ?>
            <p><code>$valeur</code> est supérieur à 0</p>
        <?php endif;?>


        <?php
        $nb_articles = 12;
        $result = "";
        switch ($nb_articles) {
        case 0:$result = 'Panier vide';
            break;
        case 1:$result = 'Votre panier contient 1 article';
            break;
        default:$result = 'Votre panier contient plusieurs articles';
            break;
        }
        ?>

        <p>Résultat du cas parmis: <code><?php echo $result; ?></code></p>

        <p>Et maintenant direction le TP 2 !</p>

    </main>


</body>

</html>