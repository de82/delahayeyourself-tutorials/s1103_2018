<?php

function showArticles($articles){

    foreach($articles as $article): ?>

    <article>
        <h3><?php echo $article['title']; ?></h3>
        <p><?php echo $article['description']; ?></p>

        <strong><?php echo $article['author']; ?></strong>
    </article>

    <?php endforeach;

}