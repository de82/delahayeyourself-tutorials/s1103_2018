<?php
    $arr_articles = array();
    $arr_articles[0] = array(
        'title' => 'Useless title',
        'description' => 'Description ..',
        'author' => 'John Doe',
    );

    $arr_articles[1] = array(
        'title' => 'Useless title 2',
        'description' => 'Description .. 2',
        'author' => 'John Doe',
    );

    $arr_articles[2] = array(
        'title' => 'Useless title 3',
        'description' => 'Description .. 3',
        'author' => 'Ben Kenobi',
    );