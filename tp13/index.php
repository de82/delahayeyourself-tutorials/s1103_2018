<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°13 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php include '_header.php';?>
    <?php require_once 'data.php'; ?>
    <?php require_once 'functions.php'; ?>

    <main>
        <section>
            <?php showArticles($arr_articles); ?>
        </section>
    </main>


</body>

</html>