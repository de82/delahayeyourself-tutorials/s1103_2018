<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°15 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php require_once 'const.php'; ?>
    <?php include '_header.php';?>

    <main>
        <h2>Hello</h2>

        <?php if(isset($_GET['firstname']) and isset($_GET['name'])): ?>
            <p>Hello <code>
            <?php echo $_GET['firstname']; ?> 
            <?php echo $_GET['name']; ?>
            </code>! </p>
        <?php else: ?>
            <p>Erreur sur les args !</p>
        <?php endif; ?>
    </main>
</body>

</html>