<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <title>S1103 - PHP TP N°15 !</title>
    <link rel="stylesheet" type="text/css" href="../assets/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../assets/style.css" />
</head>

<body>
    <?php require_once 'const.php'; ?>
    <?php include '_header.php';?>

    <main>
        <h2>Index</h2>
        <?php $names = array(
            'luke' => 'skywalker',
            'han' => 'solo',
            'ben' => 'kenobi',
        ); ?>

        <ul>
            <?php foreach($names as $firstname => $name): ?>
                <li>
                    <a href="hello.php?firstname=<?php echo $firstname; ?>&name=<?php echo $name; ?>">Say hello to <code><?php echo $firstname; ?></code></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </main>


</body>

</html>